resource "aws_subnet" "public_us_east_1a" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.pub_sub_cidr
  availability_zone = var.pub_sub_az

  tags = {
    Name = "Public Subnet us-east-1a"
  }
}

resource "aws_subnet" "public_us_east_1b" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.pri_sub_cidr
  availability_zone = var.pri_sub_az

  tags = {
    Name = "Public Subnet us-east-1b"
  }
}