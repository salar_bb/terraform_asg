terraform {
  backend "s3" {
    bucket         = "config-bucket-terraform-state"
    key            = "demo_asg/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "lock_demo_table"
  }
}