output "elb_dns_name" {
  value = aws_elb.web_elb.dns_name
}

output "vpc_id" {
    value = aws_vpc.my_vpc.id
}