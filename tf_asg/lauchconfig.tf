resource "aws_launch_configuration" "jenkins" {
  name_prefix = "jenkins"
  image_id =  var.image_id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = var.instance_type
  key_name = var.key_name
  security_groups = [ aws_security_group.allow_http.id ]
  associate_public_ip_address = true

 
}
