resource "aws_vpc" "my_vpc" {
  cidr_block       = var.vpc_id
  enable_dns_hostnames = true

  tags = {
    Name = "My VPC"
  }
}